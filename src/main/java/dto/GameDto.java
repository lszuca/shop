package dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class GameDto extends GameSummaryDto{

	private List<OrderSummaryDto> orders;

	public List<OrderSummaryDto> getOrders() {
		return orders;
	}

	public void setAuthors(List<OrderSummaryDto> authors) {
		this.orders = authors;
	}
	
	
}
