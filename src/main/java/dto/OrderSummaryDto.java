package dto;

public class OrderSummaryDto {
	
	private int invoceName;
	private int id;
	
	public int getInvoceName() {
		return invoceName;
	}
	public void setInvoceName(int invoceName) {
		this.invoceName = invoceName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
