package repo;
import java.util.List;

import model.Customer;
import model.EntityBase;
import database.MockDB;


public class RepoCustomer implements IRepository<Customer> {
	
	
	MockDB db;
	public RepoCustomer(MockDB db){
		this.db=db;
	}
	@Override
	public Customer get(int id) {
		
		return (Customer)db.get(id);
	}
	@Override
	public List<Customer> getAll() {
		
		return db.getItemsByType(Customer.class);
	}
	@Override
	public void save(EntityBase E) {
		db.save(E);
		
	}
	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}
	@Override
	public void update(EntityBase E) {
		// TODO Auto-generated method stub
		
	}
//to string

}
