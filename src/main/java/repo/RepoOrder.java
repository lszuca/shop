package repo;
import java.util.List;

import model.Order;
import model.EntityBase;
import database.MockDB;


public class RepoOrder implements IRepository<Order> {
	
	
	MockDB db;
	public RepoOrder(MockDB db){
		this.db=db;
	}
	@Override
	public Order get(int id) {
		
		return (Order)db.get(id);
	}
	@Override
	public List<Order> getAll() {
		
		return db.getItemsByType(Order.class);
	}
	@Override
	public void save(EntityBase E) {
		db.save(E);
		
	}
	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}
	@Override
	public void update(EntityBase E) {
		// TODO Auto-generated method stub
		
	}
// to string

}
