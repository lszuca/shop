package repo;
import java.util.List;

import model.Genre;
import model.EntityBase;
import database.MockDB;


public class RepoGenre implements IRepository<Genre> {
	
	
	MockDB db;
	public RepoGenre(MockDB db){
		this.db=db;
	}
	@Override
	public Genre get(int id) {
		
		return (Genre)db.get(id);
	}
	@Override
	public List<Genre> getAll() {
		
		return db.getItemsByType(Genre.class);
	}
	@Override
	public void save(EntityBase E) {
		db.save(E);
		
	}
	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}
	@Override
	public void update(EntityBase E) {
		// TODO Auto-generated method stub
		
	}
//to string

}
