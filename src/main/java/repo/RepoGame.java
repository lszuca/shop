package repo;

import java.util.List;

import model.Game;
import model.EntityBase;
import database.MockDB;


public class RepoGame implements IRepository<Game> {
	
	
	MockDB db;
	public RepoGame(MockDB db){
		this.db=db;
	}
	@Override
	public Game get(int id) {
		
		return (Game)db.get(id);
	}

	@Override
	public List<Game> getAll() {
		
		return db.getItemsByType(Game.class);
	}

	@Override
	public void save(EntityBase E) {
		db.save(E);
		
	}

	@Override
	public void delete(EntityBase E) {
		db.delete(E);
	}

	@Override
	public void update(EntityBase E) {
	}
// to string...
}
