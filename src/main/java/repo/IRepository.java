package repo;

import java.util.*;

import model.EntityBase;

public interface IRepository<E extends EntityBase>{
	public E get(int id);
	public List<E> getAll();
	public void save(EntityBase E);
	public void delete(EntityBase E);
	public void update(EntityBase E);
}