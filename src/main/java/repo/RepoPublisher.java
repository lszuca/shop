package repo;

import java.util.List;

import model.Publisher;
import model.EntityBase;
import database.MockDB;


public class RepoPublisher implements IRepository<Publisher> {
	
	
	MockDB db;
	public RepoPublisher(MockDB db){
		this.db=db;
	}
	@Override
	public Publisher get(int id) {
		
		return (Publisher)db.get(id);
	}
	@Override
	public List<Publisher> getAll() {

		return db.getItemsByType(Publisher.class);
	}
	@Override
	public void save(EntityBase E) {
		db.save(E);
		
	}
	@Override
	public void delete(EntityBase E) {
		db.delete(E);
		
	}
	@Override
	public void update(EntityBase E) {
		// TODO Auto-generated method stub
		
	}
	//to string
}
