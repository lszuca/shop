package model;
import java.util.List;




public class Order extends EntityBase {

	//private int idOrder;
	//private int idCustomer;
	private String invoiceName;
	private Customer customer;
	private List <Game> gameList;
	
	public List<Game> getGameList() {
		return gameList;
	}
	public void setGameList(List<Game> gameList) {
		this.gameList = gameList;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public String getInvoiceName() {
		return invoiceName;
	}
	public void setInvoiceName(String invoiceName) {
		this.invoiceName = invoiceName;
	}
}
