package model;
import java.util.List;




public class Publisher extends EntityBase{
	
	//private int idPublisher;
	private String publisherName;
	private List<Game> gameList;
	
	public List<Game> getGameList() {
		return gameList;
	}
	public void setGameList(List<Game> gameList) {
		this.gameList = gameList;
	}
	
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

}
