package model;
import java.util.List;




public class Genre extends EntityBase {
	
	//private int idGenre;
	private String genreName;
	private List<Game> gameList;
	
	public List<Game> getGameList() {
		return gameList;
	}
	public void setGameList(List<Game> gameList) {
		this.gameList = gameList;
	}
	
	public String getGenreName() {
		return genreName;
	}
	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}
}
