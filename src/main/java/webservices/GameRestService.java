package webservices;


import javax.ejb.Stateless;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Game;
import database.UnitOfWork;
import database.builder.GameBuilder;
import database.builder.IEntityBuilder;
import database.managers.IGameManager;
import database.managers.impl.MysqlGameManager;
import dto.GameDto;
import dto.GameSummaryDto;

@Path("Game")
@Stateless
public class GameRestService {
	
	@Inject
	private IGameManager mgr;
	
	@GET
	@Path("/giveGame/{id}")
    @Produces("application/json")
	public GameDto giveGame(@PathParam("id") int id)
	{
		//UnitOfWork uow = new UnitOfWork();
		//IEntityBuilder<Game> builder = new GameBuilder();
		//IGameManager mgr = new MysqlGameManager(uow,builder,null);
		Game b = mgr.get(id);
		GameDto result = new GameDto();
		result.setTitle(b.getTitle());
		result.setYop(b.getYop());
		
		return result;
		
	}
	
	public GameSummaryDto saveGame(GameDto request)
	{
		//UnitOfWork uow = new UnitOfWork();
		//IEntityBuilder<Game> builder = new GameBuilder();
		//IGameManager mgr = new MysqlGameManager(uow,builder,null);
		Game g = new Game();
		g.setTitle(request.getTitle());
		g.setYop(request.getYop());
		mgr.save(g);
		//uow.commit();
		mgr.saveChanges();
		GameSummaryDto result = new GameSummaryDto();
		result.setTitle(g.getTitle());
		result.setYop(g.getYop());
		
		return result;
		
	}
}
