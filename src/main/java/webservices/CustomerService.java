package webservices;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Customer;
import database.managers.ICustomerManager;
import dto.CustomerSummaryDto;

@WebService
public class CustomerService {

	@Inject
	ICustomerManager mgr;
	
	@WebMethod
	public CustomerSummaryDto giveCustomer(@PathParam("id") int id)
	{
		
		Customer c = mgr.get(id);
		
		CustomerSummaryDto res = new CustomerSummaryDto();
		res.setId(c.getId());
		res.setName(c.getName());
		res.setSurname(c.getSurname());
		
		return res;
	}
}
