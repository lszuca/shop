package webservices;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Game;
import database.UnitOfWork;
import database.builder.GameBuilder;
import database.builder.IEntityBuilder;
import database.managers.IGameManager;
import database.managers.impl.MysqlGameManager;
import dto.*;

@WebService
public class GameService {

	@Inject
	private IGameManager mgr;
	
	@WebMethod
	public GameDto giveGame(int request)
	{
		Game g = mgr.get(request);
		GameDto result = new GameDto();
		result.setTitle(g.getTitle());
		result.setYop(g.getYop());
		
		return result;
		
	}
	
	@WebMethod
	public GameSummaryDto saveGame(GameDto request)
	{
		//UnitOfWork uow = new UnitOfWork();
		//IEntityBuilder<Game> builder = new GameBuilder();
		//IGameManager mgr = new MysqlGameManager(uow,builder,null);
		Game g = new Game();
		g.setTitle(request.getTitle());
		g.setYop(request.getYop());
		mgr.save(g);
		mgr.saveChanges();
		GameSummaryDto result = new GameSummaryDto();
		result.setTitle(g.getTitle());
		result.setYop(g.getYop());
		
		return result;
		
	}
}
