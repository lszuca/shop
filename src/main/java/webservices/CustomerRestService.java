package webservices;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import model.Customer;
import database.managers.ICustomerManager;
import dto.CustomerSummaryDto;

@Path("Customer")
@Stateless
public class CustomerRestService {

	@Inject
	ICustomerManager mgr;
	
	@GET
	@Path("/giveCustomer/{id}")
    @Produces("application/json")
	public CustomerSummaryDto giveCustomer(@PathParam("id") int id)
	{
		
		Customer c = mgr.get(id);
		
		CustomerSummaryDto res = new CustomerSummaryDto();
		res.setId(c.getId());
		res.setName(c.getName());
		res.setSurname(c.getSurname());
		res.setAdress(c.getAdress());
		
		return res;
	}
}
