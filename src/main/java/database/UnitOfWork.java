package database;
import java.util.Map;
import java.util.HashMap;


import database.unitofwork.IUnitOfWork;
import database.unitofwork.IUnitOfWorkRepository;
import model.*;


public class UnitOfWork implements IUnitOfWork {
	
	private Map<EntityBase, IUnitOfWorkRepository> added;
	private Map<EntityBase, IUnitOfWorkRepository> deleted;
	private Map<EntityBase, IUnitOfWorkRepository> updated;
	
	public UnitOfWork() {
		added = new HashMap<EntityBase, IUnitOfWorkRepository>();
		deleted = new HashMap<EntityBase, IUnitOfWorkRepository>();
		updated = new HashMap<EntityBase, IUnitOfWorkRepository>();
	}
	@Override
	public void markAdded(EntityBase entity, IUnitOfWorkRepository repo){
		added.put(entity, repo);
	}
	
	@Override
	public void markDeleted(EntityBase entity, IUnitOfWorkRepository repo){
		deleted.put(entity, repo);
	}
	
	@Override
	public void markUpdated(EntityBase entity, IUnitOfWorkRepository repo){
		updated.put(entity, repo);
	}
	
	@Override
	public void commit(){
			for(EntityBase entity:added.keySet()){
				added.get(entity).persistAdded(entity);
			}
			for(EntityBase entity:deleted.keySet()){
				deleted.get(entity).persistDeleted(entity);
			
			}
			for(EntityBase entity:updated.keySet()){
				updated.get(entity).persistUpdated(entity);
			
			}
			added.clear();
			deleted.clear();
			updated.clear();
	}

	
}