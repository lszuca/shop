package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;


//import model.Order;
import model.Customer;
import model.EntityBase;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.ICustomerManager;
import database.unitofwork.IUnitOfWork;

@RequestScoped
public class MysqlCustomerManager extends ManagerBase<Customer> implements ICustomerManager{
	
	
	//private ICustomerManager CustomerMgr;
	private Connection connection;
	private String url = "";
	
	private PreparedStatement getCustomerById;
	private PreparedStatement getAllCustomers;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Customer> builder;
	
	private Statement stmt;
	@Inject
	public MysqlCustomerManager(IUnitOfWork uow, IEntityBuilder<Customer> builder) {
		super(uow);
		
		this.builder = builder;
		
		try{
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(url);
			stmt =connection.createStatement();
			
			ResultSet rs = connection.getMetaData()
					.getTables(null,null,null,null);
			
			boolean exist = false;
			while(rs.next())
			{
				if("Customer".equalsIgnoreCase(rs.getString("TABLE_NAME")))
				{
					exist=true;
					break;
				}
			}
			
			if(!exist)
			{
				
			}
			
			getCustomerById = connection.prepareStatement(""
					+ "SELECT * FROM Customer WHERE id=?");
			getAllCustomers = connection.prepareStatement("SELECT * FROM Customer LIMIT ?");
			insert = connection.prepareStatement(""
					+ "INSERT INTO Customer(name,surname,adress)"
					+ "VALUES (?,?,?)");
			delete = connection.prepareStatement(""
					+ "DELETE FROM Customer WHERE id=?");
			update = connection.prepareStatement("UPDATE Customer SET "
					+ "(name,surname,adress)=(?,?,?) "
					+ "WHERE id=?");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		
		
	}

		
	
	@Override
	public Customer get(int id) {
		Customer result = null;
		try{
			
			getCustomerById.setInt(1, id);
			ResultSet rs = getCustomerById.executeQuery();
			while(rs.next())
			{
				result = builder.build(rs);
			}
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return result;
	}
	@Override
	public List<Customer> getAll(PageInfo request) {
List<Customer> games = new ArrayList<Customer>();
		
		try{
		int n = request.getPageIndex()*request.getPageSize();
		getAllCustomers.setInt(1, n);
		getAllCustomers.setInt(2, request.getPageSize());
		ResultSet rs = getAllCustomers.executeQuery();
		while(rs.next())
		{
			Customer result = builder.build(rs);
			
			games.add(result);
		}
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	return games;// TODO na pewno games?
	}
	@Override
	public void persistAdded(EntityBase entity) {
		Customer b = (Customer)entity;
		try {
			
			insert.setString(1, b.getName());
			insert.setString(1, b.getSurname());
			insert.setString(1, b.getAdress());
			insert.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void persistDeleted(EntityBase entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void persistUpdated(EntityBase entity) {
		Customer b = (Customer)entity;
		try{
			update.setString(1, b.getName());
			update.setString(2, b.getSurname());
			update.setString(3, b.getAdress());
			update.setInt(4, entity.getId());
			update.executeUpdate();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
}