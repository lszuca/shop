package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Game;
import model.Publisher;
import model.EntityBase;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IPublisherManager;
import database.unitofwork.IUnitOfWork;

@RequestScoped
public class MysqlPublisherManager extends ManagerBase<Publisher> implements IPublisherManager{
	
	
	//private IPublisherManager publisherMgr;
	private Connection connection;
	private String url = "";
	
	private PreparedStatement getPublisherById;
	private PreparedStatement getAllPublishers;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Publisher> builder;
	
	private Statement stmt;
	@Inject
	public MysqlPublisherManager(IUnitOfWork uow, IEntityBuilder<Publisher> builder) {
		super(uow);
		
		this.builder = builder;
		
		try{
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(url);
			stmt =connection.createStatement();
			
			ResultSet rs = connection.getMetaData()
					.getTables(null,null,null,null);
			
			boolean exist = false;
			while(rs.next())
			{
				if("Publisher".equalsIgnoreCase(rs.getString("TABLE_NAME")))
				{
					exist=true;
					break;
				}
			}
			
			if(!exist)
			{
				
			}
			
			getPublisherById = connection.prepareStatement(""
					+ "SELECT * FROM publisher WHERE id=?");
			getAllPublishers = connection.prepareStatement("SELECT * FROM publisher LIMIT ?");
			insert = connection.prepareStatement(""
					+ "INSERT INTO publisher(publisherName)"
					+ "VALUES (?)");
			delete = connection.prepareStatement(""
					+ "DELETE FROM publisher WHERE id=?");
			update = connection.prepareStatement("UPDATE publisher SET "
					+ "(publisherName)=(?) "
					+ "WHERE id=?");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		
		
	}
	@Override
	public void setGameList(List<Game> gameList) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Publisher get(int id) {
		Publisher result = null;
		try{
			
			getPublisherById.setInt(1, id);
			ResultSet rs = getPublisherById.executeQuery();
			while(rs.next())
			{
				result = builder.build(rs);
			}
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return result;
	}
	@Override
	public List<Publisher> getAll(PageInfo request) {
List<Publisher> games = new ArrayList<Publisher>();
		
		try{
		int n = request.getPageIndex()*request.getPageSize();
		getAllPublishers.setInt(1, n);
		getAllPublishers.setInt(2, request.getPageSize());
		ResultSet rs = getAllPublishers.executeQuery();
		while(rs.next())
		{
			Publisher result = builder.build(rs);
			
			games.add(result);
		}
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	return games;
	}
	@Override
	public void persistAdded(EntityBase entity) {
		Publisher b = (Publisher)entity;
		try {
			
			insert.setString(1, b.getPublisherName());
			insert.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void persistDeleted(EntityBase entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void persistUpdated(EntityBase entity) {
		Publisher b = (Publisher)entity;
		try{
			update.setString(1, b.getPublisherName());
			update.setInt(4, entity.getId());
			update.executeUpdate();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
}
