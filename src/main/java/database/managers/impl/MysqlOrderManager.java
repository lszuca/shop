package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Game;
import model.Order;
import model.EntityBase;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IOrderManager;
import database.unitofwork.IUnitOfWork;

@RequestScoped
public class MysqlOrderManager extends ManagerBase<Order> implements IOrderManager{
	
	
	//private IOrderManager OrderMgr;
	private Connection connection;
	private String url = "";
	
	private PreparedStatement getOrderById;
	private PreparedStatement getAllOrders;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Order> builder;
	
	private Statement stmt;
	@Inject
	public MysqlOrderManager(IUnitOfWork uow, IEntityBuilder<Order> builder) {
		super(uow);
		
		this.builder = builder;
		
		try{
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(url);
			stmt =connection.createStatement();
			
			ResultSet rs = connection.getMetaData()
					.getTables(null,null,null,null);
			
			boolean exist = false;
			while(rs.next())
			{
				if("Order".equalsIgnoreCase(rs.getString("TABLE_NAME")))
				{
					exist=true;
					break;
				}
			}
			
			if(!exist)
			{
				
			}
			
			getOrderById = connection.prepareStatement(""
					+ "SELECT * FROM Order WHERE id=?");
			getAllOrders = connection.prepareStatement("SELECT * FROM Order LIMIT ?");
			insert = connection.prepareStatement(""
					+ "INSERT INTO Order(invoiceName)"
					+ "VALUES (?)");
			delete = connection.prepareStatement(""
					+ "DELETE FROM Order WHERE id=?");
			update = connection.prepareStatement("UPDATE Order SET "
					+ "(invoiceName)=(?) "
					+ "WHERE id=?");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		
		
	}
	@Override
	public void setGameList(List<Game> gameList) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Order get(int id) {
		Order result = null;
		try{
			
			getOrderById.setInt(1, id);
			ResultSet rs = getOrderById.executeQuery();
			while(rs.next())
			{
				result = builder.build(rs);
			}
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return result;
	}
	@Override
	public List<Order> getAll(PageInfo request) {
List<Order> games = new ArrayList<Order>();
		
		try{
		int n = request.getPageIndex()*request.getPageSize();
		getAllOrders.setInt(1, n);
		getAllOrders.setInt(2, request.getPageSize());
		ResultSet rs = getAllOrders.executeQuery();
		while(rs.next())
		{
			Order result = builder.build(rs);
			
			games.add(result);
		}
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	return games;
	}
	@Override
	public void persistAdded(EntityBase entity) {
		Order b = (Order)entity;
		try {
			
			insert.setString(1, b.getInvoiceName());
			insert.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void persistDeleted(EntityBase entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void persistUpdated(EntityBase entity) {
		Order b = (Order)entity;
		try{
			update.setString(1, b.getInvoiceName());
			update.setInt(4, entity.getId());
			update.executeUpdate();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
}