package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Game;
import model.EntityBase;
			//import model.Genre;
//import model.Order;
//import model.Publisher;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
//import database.managers.IOrderManager;
import database.managers.IGameManager;
//import database.managers.IPublisherManager;
//import database.managers.IGenreManager;
import database.unitofwork.IUnitOfWork;

@RequestScoped
public class MysqlGameManager extends ManagerBase<Game> implements IGameManager{
	/*
	private IOrderManager orderMgr;
	private IPublisherManager publisherMgr;
	private IGenreManager genreMgr;
	*/
	private Connection connection;
	private String url = "";
	
	private PreparedStatement getGamesById;
	private PreparedStatement getAllGames;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Game> builder;
	/*private PreparedStatement getPublisher;
	private PreparedStatement getOrder;
	private PreparedStatement getGenre;
	*/
	private Statement stmt;
	
	
	@Inject
	public MysqlGameManager(IUnitOfWork uow, 
			IEntityBuilder<Game> builder
			//IPublisherManager publisherMgr, IOrderManager orderMgr,
			//IGenreManager genreMgr
			) {
		super(uow);
		
		this.builder=builder;
		/*
		this.publisherMgr=publisherMgr;
		this.orderMgr=orderMgr;
		this.genreMgr=genreMgr;
		*/
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(url);
			stmt = connection.createStatement();
			ResultSet rs = connection.getMetaData()
					.getTables(null, null, null, null);
			
			boolean exist = false;
			while(rs.next()){
				if ("Game".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
					exist = true;
					break;
				}
			}
			if(!exist){
				stmt.executeUpdate(""
						+"CREATE TABLE Game("
						+"id int(10) ,"
						+"title VARCHAR (20),"
						+"yop int(4)"
						+")"
						+"");	
			}
			getGamesById = connection.prepareStatement(""
					+ "SELECT * FROM Game WHERE id=?");
			getAllGames = connection.prepareStatement("SELECT * FROM Game LIMIT ? ?");
			insert = connection.prepareStatement(""
					+"INSERT INTO Game(title,yop)"
					+"VALUES (?,?)");
			delete = connection.prepareStatement(""
					+"DELETE FROM Game WHERE id=?");
			update = connection.prepareStatement("UPDATE Game SET"
					+ "(title,yop)=(?,?) "
					+"WHERE id=?");		
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}



	@Override
	public Game get(int id) {
		Game result = null;
		try{
			getGamesById.setInt(1,id);
			ResultSet rs = getGamesById.executeQuery();
			while(rs.next()){
				result = builder.build(rs);
			}
		}
			catch(Exception ex){
				ex.printStackTrace();
			}		
		return result;
	}

	@Override
	public List<Game> getAll(PageInfo request) {
		List<Game> games = new ArrayList<Game>();
		try{
			int n = request.getPageIndex()*request.getPageSize();
			getAllGames.setInt(1, n);
			getAllGames.setInt(2, request.getPageSize());
			ResultSet rs = getAllGames.executeQuery();
			while(rs.next()){
				Game result = builder.build(rs);
				
				games.add(result);
			}
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return games;
	}

	@Override
	public void persistAdded(EntityBase entity) {
		Game g = (Game)entity;
		try {
			
			insert.setString(1, g.getTitle());
			insert.setInt(3, g.getYop());
			insert.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void persistDeleted(EntityBase entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void persistUpdated(EntityBase entity) {
		Game g = (Game)entity;
		try{
			update.setString(1, g.getTitle());
			update.setInt(2, g.getYop());
			update.setInt(3, entity.getId());
			update.executeUpdate();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}



	
	
	
	
}



