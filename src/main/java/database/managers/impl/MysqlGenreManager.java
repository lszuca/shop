package database.managers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import model.Game;
import model.Genre;
import model.EntityBase;
import database.ManagerBase;
import database.PageInfo;
import database.builder.IEntityBuilder;
import database.managers.IGenreManager;
import database.unitofwork.IUnitOfWork;

@RequestScoped
public class MysqlGenreManager extends ManagerBase<Genre> implements IGenreManager{
	
	
	//private IGenreManager GenreMgr;
	private Connection connection;
	private String url = "";
	
	private PreparedStatement getGenreById;
	private PreparedStatement getAllGenres;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement insert;
	private IEntityBuilder<Genre> builder;
	
	private Statement stmt;
	@Inject
	public MysqlGenreManager(IUnitOfWork uow, IEntityBuilder<Genre> builder) {
		super(uow);
		
		this.builder = builder;
		
		try{
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(url);
			stmt =connection.createStatement();
			
			ResultSet rs = connection.getMetaData()
					.getTables(null,null,null,null);
			
			boolean exist = false;
			while(rs.next())
			{
				if("Genre".equalsIgnoreCase(rs.getString("TABLE_NAME")))
				{
					exist=true;
					break;
				}
			}
			
			if(!exist)
			{
				
			}
			
			getGenreById = connection.prepareStatement(""
					+ "SELECT * FROM Genre WHERE id=?");
			getAllGenres = connection.prepareStatement("SELECT * FROM Genre LIMIT ?");
			insert = connection.prepareStatement(""
					+ "INSERT INTO Genre(GenreName)"
					+ "VALUES (?)");
			delete = connection.prepareStatement(""
					+ "DELETE FROM Genre WHERE id=?");
			update = connection.prepareStatement("UPDATE Genre SET "
					+ "(GenreName)=(?) "
					+ "WHERE id=?");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		
		
	}
	@Override
	public void setGameList(List<Game> gameList) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Genre get(int id) {
		Genre result = null;
		try{
			
			getGenreById.setInt(1, id);
			ResultSet rs = getGenreById.executeQuery();
			while(rs.next())
			{
				result = builder.build(rs);
			}
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return result;
	}
	@Override
	public List<Genre> getAll(PageInfo request) {
List<Genre> games = new ArrayList<Genre>();
		
		try{
		int n = request.getPageIndex()*request.getPageSize();
		getAllGenres.setInt(1, n);
		getAllGenres.setInt(2, request.getPageSize());
		ResultSet rs = getAllGenres.executeQuery();
		while(rs.next())
		{
			Genre result = builder.build(rs);
			
			games.add(result);
		}
		
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	return games;
	}
	@Override
	public void persistAdded(EntityBase entity) {
		Genre b = (Genre)entity;
		try {
			
			insert.setString(1, b.getGenreName());
			insert.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void persistDeleted(EntityBase entity) {
		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void persistUpdated(EntityBase entity) {
		Genre b = (Genre)entity;
		try{
			update.setString(1, b.getGenreName());
			update.setInt(4, entity.getId());
			update.executeUpdate();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
}
