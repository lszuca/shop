package database.managers;

import model.Publisher;
import model.Game;
import java.util.List;
import database.IManager;

public interface IPublisherManager extends IManager<Publisher> {
	
	public void setGameList(List<Game> gameList);

}
