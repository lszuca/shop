package database.managers;

import model.Customer;
import model.Order;
import database.IManager;

public interface ICustomerManager extends IManager<Customer>{

	public void setOrderList(Order b);
}
