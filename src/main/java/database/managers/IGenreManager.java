package database.managers;

import model.Genre;
import model.Game;
import java.util.List;
import database.IManager;

public interface IGenreManager extends IManager<Genre>{

	public void setGameList(List<Game> gameList);
}
