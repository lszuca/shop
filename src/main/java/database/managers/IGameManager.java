package database.managers;

import model.Order;
import model.Game;
import model.Publisher;
import model.Genre;

import java.util.List;

import database.IManager;
import database.PageInfo;


public interface IGameManager extends IManager<Game> {
	
	public void setOrderList(List<Order> orderList);
	public void setPublisher(Publisher p);
	public void setGenre (Genre g);
	List<Game> getAll(PageInfo request);
}
