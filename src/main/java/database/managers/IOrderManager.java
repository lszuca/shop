package database.managers;

import java.util.List;

import model.Order;
import model.Game;
import database.IManager;

public interface IOrderManager extends IManager<Order>{

	public void setGameList(List<Game> gameList);
}
