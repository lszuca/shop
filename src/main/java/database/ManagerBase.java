package database;
import java.util.List;

import database.unitofwork.IUnitOfWork;
import database.unitofwork.IUnitOfWorkRepository;
import model.*;

public abstract class ManagerBase<E extends EntityBase> implements IManager<E>, IUnitOfWorkRepository {
	
		protected IUnitOfWork uow;
		
		public ManagerBase(IUnitOfWork uow) {
			super();
			this.uow = uow;
		}
		
		@Override
		public void save (E object){
			uow.markAdded(object, this);
		}
		
		@Override
		public void delete (E object){
			uow.markDeleted(object, this);
		}
		
		@Override
		public void update (E object){
			uow.markUpdated(object, this);
		}
		
		public final void SaveChanges(){
			uow.commit();
		}
		
		@Override
		public abstract E get(int id);
		
		@Override
		public abstract List<E> getAll();
		
		@Override
		public abstract void persistAdded (EntityBase entity);
		
		@Override
		public abstract void persistDeleted (EntityBase entity);
		
		@Override
		public abstract void persistUpdated (EntityBase entity);

}
