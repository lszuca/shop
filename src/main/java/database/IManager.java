package database;

import java.util.List;
import model.*;

public interface IManager <E extends EntityBase> {

	public E get (int item);
	public List<E> getAll();
	public void save (E object);
	public void delete (E object);
	public void update (E object);
}
