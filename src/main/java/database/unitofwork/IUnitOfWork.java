package database.unitofwork;
import model.*;

public interface IUnitOfWork {

	public void markAdded(EntityBase entity, IUnitOfWorkRepository repo);
	public void markDeleted(EntityBase entity, IUnitOfWorkRepository repo);
	public void markUpdated(EntityBase entity, IUnitOfWorkRepository repo);
	public void commit();
}
