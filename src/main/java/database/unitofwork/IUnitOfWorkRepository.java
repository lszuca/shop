package database.unitofwork;
import model.*;

public interface IUnitOfWorkRepository {
	public void persistAdded(EntityBase entity);
	public void persistDeleted(EntityBase entity);
	public void persistUpdated(EntityBase entity);

}
