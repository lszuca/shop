package database;

import java.util.ArrayList;
import java.util.List;

import database.*;
import model.*;


public class MockDB {

		private ArrayList<EntityBase> entitylist = new ArrayList<EntityBase>();
		private int size;
		
		public ArrayList<EntityBase> getEntityList(){
			return entitylist;
		}
		
		public EntityBase get(int id){
			return entitylist.get(id);
		}
		
		public void save(EntityBase item){
			size++;
			item.setId(size);
			entitylist.add(item);
		}
		
		public void delete(EntityBase item){
			entitylist.remove(item);
		}
		
		public <T extends EntityBase> List<T> getItemsByType(Class<T> c){
			List<T> result = new ArrayList<T>();
			for(EntityBase item: entitylist){
				
					if(item.getClass().getName().equals(c.getName())){
						result.add((T)item);
					}
			}
			return result;
		}
	}
