package database.builder;

import java.sql.ResultSet;
import model.Game;

public class GameBuilder implements IEntityBuilder<Game> {

	@Override
	public Game build(ResultSet rs) {
		Game result = null;
		try{
			result = new Game();
			result.setId(rs.getInt("id"));
			result.setTitle(rs.getString("title"));
			result.setYop(rs.getInt("yop"));
			// TODO result.setPublisher(rs.getString("publisher"));
			// TODO result.setGenre(rs.getString("genre"));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return result;
	}

}
