package database.builder;

import java.sql.ResultSet;
import model.Customer;

public class CustomerBuilder implements IEntityBuilder<Customer>{

	@Override
	public Customer build(ResultSet rs) {
		Customer a = null;
		try{
			a = new Customer();
			a.setId(rs.getInt("id"));
			a.setName(rs.getString("name"));
			a.setSurname(rs.getString("surename"));
			a.setAdress(rs.getString("adress"));
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
		return a;
		}
	}


