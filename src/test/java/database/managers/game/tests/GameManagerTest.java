package database.managers.game.tests;

import static org.junit.Assert.*;
import model.Game;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import database.MockDB;
import database.UnitOfWork;
import database.managers.IGameManager;
import database.managers.impl.MysqlGameManager;
import database.unitofwork.IUnitOfWork;

public class GameManagerTest {
	
	private  MockDB db;
	private IUnitOfWork uow;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		db = new MockDB();
		Game g1 = new Game();
		Game g2 = new Game();
		Game g3 = new Game();
		Game g4 = new Game();
		
		g1.setTitle("AC");
		g1.setYop(2010);
		
		
		g2.setTitle("BF2");
		g2.setYop(2011);
		
	
		g3.setTitle("HF3");
		g3.setYop(2012);
		
		
		g4.setTitle("WD");
		g4.setYop(2013);
		
		db.save(g1);
		db.save(g2);
		db.save(g3);
		db.save(g4);
		
		uow = new UnitOfWork();
		
	}

	@After
	public void tearDown() throws Exception {
		db.getEntityList().clear();
	}

	@Test
	public void testPersistAdd() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistDeleted() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistUpdated() {
		fail("Not yet implemented");
	}

	@Test
	public void test_sprawdza_poprawnosc_wyciagania_ksiazki_z_bazy() {
		IGameManager mgr = new MysqlGameManager(uow, builder)//TODO
		Game game = mgr.get(1);
		Game game2 = mgr.get(1);
		assertNotNull("nie udało się wybrać gry z bazy"
				,game);
		assertEquals("problem z tytułem",
				game.getTitle(),"AC");
		assertEquals("problem z rokiem",
				game.getYop(),"2010");
		assertNotSame("obiekty wskazują na tą samą przestrzeń w pamieci"
				,game,game);
	}

	@Test
	public void testEntityList() {
		fail("Not yet implemented");
	}

}
